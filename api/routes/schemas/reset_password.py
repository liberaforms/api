from marshmallow import Schema, fields
class ResetPasswordSchema(Schema):
    password = fields.Str(load_only=True, required=True)
    reset_token = fields.Str(required=True)
