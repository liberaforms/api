from marshmallow import Schema, fields, EXCLUDE


class MetaSchema(Schema):
    class Meta:
        # EXCLUDE: unknown fields will dropped (not be parsed)
        unknown = EXCLUDE
    count = fields.Int(dump_only=True)
