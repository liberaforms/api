"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

from marshmallow import Schema, fields


class FormSchema(Schema):
    id = fields.Int()
    createdAt = fields.DateTime()
    updatedAt = fields.DateTime()
    createdBy = fields.Str()
    
    name = fields.Str(required=True)
    description = fields.Str()
    schema = fields.Dict(required=True)
    href = fields.Str()
