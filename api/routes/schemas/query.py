from marshmallow import Schema, fields


class CollectionQuerySchema(Schema):
    # missing specifies the default deserialization value for a field
    size = fields.Int(missing=10)
    page = fields.Int(missing=1)
