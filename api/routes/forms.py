import datetime
import uuid
from flask import request, jsonify
from flask_jwt_extended import jwt_required, decode_token, get_current_user
from flask_restful import Resource
from marshmallow import ValidationError

from api import db
from api.routes import rest
from api.models.form import Form as FormModel

from .schemas.query import CollectionQuerySchema
from .schemas.output import MetaSchema
from .schemas.form import FormSchema

collection_query_schema = CollectionQuerySchema()
meta_schema = MetaSchema()


class Forms(Resource):
    @jwt_required
    def get(self):
        try:
            query = collection_query_schema.load(request.args)
        except ValidationError as err:
            return err.messages, 422
        user = get_current_user()
        forms = FormModel.query.filter_by(
            createdBy=user['id']).paginate(query['page'], query['size'])
        forms_schema = FormSchema(many=True)
        items = forms_schema.dump(forms.items)
        meta = meta_schema.dump({'count': forms.total})
        return {
            'items': items,
            'meta': meta
        }, 200

    @jwt_required
    def post(self):
        user = get_current_user()
        form_schema = FormSchema(exclude=['id'])
        try:
            json = form_schema.load(request.get_json())
        except ValidationError as err:
            return err.messages, 422

        href = 'http://tbd.com'
        form = FormModel(
            created_by=user['id'],
            name=json['name'],
            description=json['description'],
            schema=json['schema'],
            href=href
        )
        try:
            db.session.add(form)
            db.session.commit()
            return {'id': form.id}, 201
        except IntegrityError as exception_message:
            return {
                'message': str(exception_message)
            }, 400


class Form(Resource):
    @jwt_required
    def get(self, id):
        form = FormModel.query.filter_by(id=id).first()
        form_schema = FormSchema()
        if form:
            result = form_schema.dump(form)
            return result, 200
        return "Not found", 404

    @jwt_required
    def put(self, id):
        form = FormModel.query.filter_by(id=id).first()
        if form:
            form_schema = FormSchema(only=("name", "description", "schema"))
            try:
                json = form_schema.load(request.get_json())
            except ValidationError as err:
                return err.messages, 422

            form.name = json['name']
            form.description = json['description']
            form.schema = json['schema']
            form.updatedAt = datetime.datetime.now().isoformat()

            try:
                db.session.commit()
                return {'id': form.id}, 201
            except IntegrityError as exception_message:
                return {
                    'message': str(exception_message)
                }, 400
            return "OK", 204
        return "Not found", 404

    @jwt_required
    def delete(self, id):
        form = FormModel.query.filter_by(id=id).first()
        if (form):
            db.session.delete(form)
            db.session.commit()
            return "OK", 204
        return "Not found", 404


rest.add_resource(Forms, '/forms')
rest.add_resource(Form, '/forms/<id>')
