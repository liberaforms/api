"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
from flask import request
from flask_jwt_extended import (jwt_required, get_current_user,
                                create_access_token, decode_token)
from flask_restful import Resource
from marshmallow import ValidationError

# TODO consider wrapping alchemy expcetions
from sqlalchemy.exc import IntegrityError

from api import email_server
from api import db
from api.routes import rest, api_bp
from api.models.user import User as UserModel
from api.routes.auth import user_has_roles

from .schemas.user import UserSchema
from .schemas.query import CollectionQuerySchema
from .schemas.output import MetaSchema

user_schema = UserSchema()
users_schema = UserSchema(many=True)
collection_query_schema = CollectionQuerySchema()
meta_schema = MetaSchema()

class Users(Resource):
    @user_has_roles(['admin'])
    def get(self):
        query = collection_query_schema.load(request.args)
        users = UserModel.query.paginate(query['page'], query['size'])
        items = users_schema.dump(users.items)
        meta = meta_schema.dump({'count': int(users.total)})
        return {
            'items': items,
            'meta': meta
        }, 200
    @user_has_roles(['admin'])
    def post(self):
        try:
            json = user_schema.load(request.get_json())
        except ValidationError as err:
            return err.messages, 422

        current_user = get_current_user()
        user = UserModel(
            createdBy=current_user['id'],
            username=json['username'],
            email=json['email'],
            group=json['group']
        )
        user.set_password(json['password'])
        try:
            db.session.add(user)
            db.session.commit()
            return {'id': user.id }, 201
        except IntegrityError as exception_message:
            return {
                'message': str(exception_message)
            }, 400


class User(Resource):
    @user_has_roles(['admin'])
    def get(self, id):
        user = UserModel.query.filter_by(id=id).first()
        res = user_schema.dump(user)
        return res, 200
    @jwt_required
    def patch(self, id):
        logged_user = get_current_user()
        if id != logged_user['id']:
            return  {
                'message': 'ID does not match logged_user.id'
            }, 422
        json = UserSchema().load(request.get_json())
        if 'email' in json:
            link = validate_email(json['email'], logged_user['id'])
            sent_status = email_server.send_email_validation(json['email'], link)
            return sent_status, 200
        if 'token' in json:
            try:
                token = decode_token(json['token'])
            except Exception as error:
                return {
                    "message": str(error)
                }, 422
            status = change_email(token['identity'], logged_user['id'])
            return status
        return "Not implemented", 400

class AuthUser(Resource):
    @jwt_required
    def get(self):
        loggedUser = get_current_user()
        user = UserModel.query.filter_by(id=loggedUser['id']).first()
        res = user_schema.dump(user)
        return res, 200


@api_bp.route('/users/exists', methods=['POST'])
def username_exists():
    # TODO. Use schema.load
    json = request.get_json()
    username = json['username']
    if not username:
        return {
            'message': 'Username is required'
        }, 422

    user = UserModel.query.filter_by(username=username).first()
    if user:
        return {
            'exists': True
        }, 200
    return {
        'exists': False
    }, 200

def validate_email(new_email, userId):
    if UserModel.query.filter_by(email=new_email).first():
        return {
            'message': 'Email is already registered.'
        }
    user = UserModel.query.filter_by(id=userId).first()
    expires = datetime.timedelta(hours=24)
    token = create_access_token(new_email, expires_delta=expires)
    # TODO use calllbackURL from payload
    link = f'{request.referrer}/{token}'
    return link

def change_email(new_email, userId):
    if UserModel.query.filter_by(email=new_email).first():
        return  {
            'message': 'Email is already registered.'
        }, 422
    user = UserModel.query.filter_by(id=userId).first()
    user.email = new_email
    db.session.commit()
    return {'email': new_email }, 200


rest.add_resource(Users, '/users')
rest.add_resource(AuthUser, '/users/me')
rest.add_resource(User, '/users/<id>')
