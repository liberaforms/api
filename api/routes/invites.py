"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
from urllib.parse import urlparse
from flask import request, current_app
from flask_jwt_extended import jwt_required, get_current_user
from flask_restful import Resource
from marshmallow import ValidationError

from api import email_server
from api import db
# TODO consider wrapping alchemy expcetions
from sqlalchemy.exc import IntegrityError

from .utils import utils

from api.routes import rest, api_bp
from api.routes.auth import user_has_roles
from api.models.invite import Invite as InviteModel
from api.models.user import User as UserModel

from .schemas.invite import InviteSchema
from .schemas.query import CollectionQuerySchema
from .schemas.output import MetaSchema

invite_schema = InviteSchema()
invites_schema = InviteSchema(many=True)
collection_query_schema = CollectionQuerySchema()
meta_schema = MetaSchema()

class Invites(Resource):
    @user_has_roles(['admin'])
    def get(self):
        try:
            query = collection_query_schema.load(request.args)
        except ValidationError as err:
            return err.messages, 422

        invites = InviteModel.query.paginate(query['page'], query['size'])
        items = invites_schema.dump(invites.items)
        meta = meta_schema.dump({'count': InviteModel.query.count()})
        return {
            'items': items,
            'meta': meta
        }, 200


    @user_has_roles(['admin'])
    def post(self):
        try:
            input_data = InviteSchema().load(request.get_json())
        except ValidationError as err:
            return err.messages, 422
        
        user = UserModel.query.filter_by(email=input_data['email']).first()
        if user:
            return {
                'message': 'User already exists'
            }, 400
        invite = InviteModel.query.filter_by(email=input_data['email']).first()
        if invite:
            return {
                'message': 'User has already been invited'
            }, 400
        user = get_current_user()
        invite = InviteModel(   input_data['email'],
                                input_data['group'],
                                user['id'],
                                input_data['message']
                            )
        try:
            db.session.add(invite)
            db.session.commit()
        except IntegrityError as exception_message:
            return {
                'message': str(exception_message)
            }, 400
        base_url = request.referrer[:-1]
        path = current_app.config['INVITES_PATH']
        invitation_link = f'{base_url}{path}/{invite.token}'
        sent_status = email_server.send_invite(invite, invitation_link)
        return {'id': invite.id, **sent_status }, 201


class Invite(Resource):
    @user_has_roles(['admin'])
    def get(self, id):
        invite = InviteModel.query.filter_by(id=id).first()
        if invite:
            fields = InviteSchema().dump(invite)
            return fields, 200
        return "Not found", 404

    @user_has_roles(['admin'])
    def delete(self, id):
        invite = InviteModel.query.filter_by(id=id).first()
        if invite:
            db.session.delete(invite)
            db.session.commit()
            return "OK", 204
        return "Not found", 404

# TODO add schema for validation
@api_bp.route('/invites/accept', methods=['POST'])
def accept_invite():
    json = request.get_json()
    invite = InviteModel.query.filter_by(token=json['token']).first()
    if not invite:
        return {
            'message': 'Invitation does not exist'
        }, 400
    
    user = UserModel(
        createdBy=invite.createdBy,
        username=json['username'],
        email=invite.email,
        group=invite.group
    )
    user.set_password(json['password'])
    try:
        db.session.add(user)
        db.session.delete(invite)
        db.session.commit()
        return {'id': user.id}, 200
    except IntegrityError as exception_message:
        return {
            'message': str(exception_message)
        }, 400

rest.add_resource(Invites, '/invites')
rest.add_resource(Invite, '/invites/<id>')
