"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
from uuid import uuid4
from urllib.parse import urlparse
from functools import wraps
from flask import jsonify, request, current_app
from flask_restful import Resource
from flask_jwt_extended import (
    create_access_token,
    create_refresh_token,
    decode_token,
    jwt_refresh_token_required,
    get_current_user,
    get_jwt_identity,
    verify_jwt_in_request
)
from sqlalchemy.exc import IntegrityError as SQLIntegrityError
from marshmallow import ValidationError

from api import jwt, db, email_server
from api.models.user import User
from api.routes import api_bp, rest
from api.routes.schemas.user import UserSchema
from api.routes.schemas.reset_password import ResetPasswordSchema
from .utils import utils

def map_user(user):
    roles = user.group.strip().split(",")
    return {
        'id': str(user.id),
        'username': user.username,
        'email': user.email,
        'createdAt': user.createdAt,
        # 'validatedEmail': user.validatedEmail,
        # 'hostname': user.hostname,
        # 'preferences': user.preferences,
        'roles': roles
    }

@jwt.user_loader_callback_loader
def user_loader(identity):
    return identity


def user_has_roles(roles):
    def required_roles(fn):
        @wraps(fn)
        def wrapper(*args, **kwargs):
            verify_jwt_in_request()
            user = get_current_user()
            for role in roles:
                if role not in user['roles']:
                    return 'Forbidden', 403
            else:
                return fn(*args, **kwargs)

        return wrapper
    return required_roles

DEFAULT_AUTH_ERROR_MSG = "Username or Password Error"

# Authentication resource
# post: login method to create access tokens and refresh tokens
class Authenitcation(Resource):
    def post(self):
        json = UserSchema(only=("username", "password")).load(request.get_json())
        user = User.query.filter_by(username=json['username']).first()
        if user and user.verify_password(json['password']):
            identity = map_user(user)
            ret = {
                'access_token': create_access_token(identity=identity),
                'refresh_token': create_refresh_token(identity=identity),
                'group': user.group
            }
            return ret, 200
        else:
            return DEFAULT_AUTH_ERROR_MSG, 401


# The jwt_refresh_token_required decorator insures a valid refresh
# token is present in the request before calling this endpoint. We
# can use the get_jwt_identity() function to get the identity of
# the refresh token, and use the create_access_token() function again
# to make a new access token for this identity.
@api_bp.route('/auth/refresh', methods=['POST'])
@jwt_refresh_token_required
def refresh():
    current_user = get_jwt_identity()
    ret = {'access_token': create_access_token(identity=current_user)}
    return jsonify(ret), 200


# Fresh login endpoint. This is designed to be used if we need to
# make a fresh token for a user (by verifying they have the
# correct username and password). Unlike the standard login endpoint,
# this will only return a new access token, so that we don't keep
# generating new refresh tokens, which entirely defeats their point.
# routes can be protected with @fresh_jwt_required decorator
@api_bp.route('/auth/fresh-login', methods=['POST'])
def fresh_login():
    try:
        json = UserSchema(only=("username", "password")).load(request.get_json())
    except ValidationError as err:
        return err.messages, 422
    user = User.query.filter_by(username=json['username']).first()
    if user and user.verify_password(json['password']):
        identity = map_user(user)
        new_token = create_access_token(identity=identity, fresh=True)
        ret = {'access_token': new_token}
        return jsonify(ret), 200
    else:
        return jsonify(DEFAULT_AUTH_ERROR_MSG), 401


# Recover password endpoint
@api_bp.route('/auth/forget', methods=['POST'])
def forget_password():
    try:
        json = UserSchema(only=("email",)).load(request.get_json())
    except ValidationError as err:
        return err.messages, 422
    user = User.query.filter_by(email=json['email']).first()
    if not user:
        return {
            'message': "email was not found"
        }, 400
    expires = datetime.timedelta(hours=24)
    reset_token = create_access_token(str(user.id), expires_delta=expires)
    base_url = request.referrer[:-1]
    path = current_app.config['RECOVER_PASSWORD_PATH']
    recovery_link = f'{base_url}{path}/{reset_token}'
    sent_status = email_server.send_account_recovery(user.email, recovery_link)
    return sent_status, 200

@api_bp.route('/auth/reset', methods=['POST'])
def reset_password():
    try:
        json = ResetPasswordSchema().load(request.get_json())
    except ValidationError as err:
        return err.messages, 422
    
    user_id = decode_token(json['reset_token'])['identity']
    user = User.query.get(user_id)
    user.set_password(json['password'])
    
    db.session.commit()

    sent_status = email_server.send_password_success(user.email)
    return sent_status, 200



rest.add_resource(Authenitcation, '/auth/login')
