from flask_jwt_extended import get_current_user
from flask_restful import Resource

from api.routes.auth import user_has_roles
from api.routes import rest

from api import email_server


class TestSMTP(Resource):
    @user_has_roles(['admin'])
    def post(self):
        admin_email = get_current_user()['email']
        sent_status = email_server.send_test_email(recipient_email=admin_email)
        return sent_status, 200


rest.add_resource(TestSMTP, '/test-smtp')
