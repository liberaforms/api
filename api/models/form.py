from sqlalchemy_json import mutable_json_type
import datetime
from sqlalchemy.dialects.postgresql import JSONB
from api import db
from . import DictMixIn

class Form(db.Model, DictMixIn):
    __tablename__ = "Forms"

    id = db.Column(db.Integer, primary_key=True, index=True)
    createdAt = db.Column(db.DateTime)
    updatedAt = db.Column(db.DateTime)
    createdBy = db.Column(db.Integer, db.ForeignKey('Users.id'))

    name = db.Column(db.String(120))
    description = db.Column(db.String(1000))
    schema = db.Column(mutable_json_type(dbtype=JSONB, nested=True))
    href = db.Column(db.String(2000))

    def __init__(self, created_by, name, description, schema, href):
        self.createdAt = datetime.datetime.now().isoformat()
        self.createdBy = created_by
        self.name = name
        self.description = description
        self.schema = schema
        self.href = href
