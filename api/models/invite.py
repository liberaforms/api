"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
from uuid import uuid4
from sqlalchemy.orm import relationship
from api import db
from . import DictMixIn

class Invite(db.Model, DictMixIn):
    __tablename__ = "Invites"

    id = db.Column(db.Integer, primary_key=True, index=True)
    createdAt = db.Column(db.DateTime)
    createdBy = db.Column(db.Integer, db.ForeignKey('Users.id'))
    message = db.Column(db.String)
    email = db.Column(db.String, unique=True)
    token = db.Column(db.String, unique=True)
    # TODO consider use enumtables
    ## comma separated Strings are valid ie: admin, user, owner
    group = db.Column(db.String)
    creator = relationship("User", back_populates="invitations")


    def __init__(self, email, group, created_by, message):
        self.email = email
        self.group = group
        self.message = message
        self.createdAt = datetime.datetime.now().isoformat()
        self.createdBy = created_by
        self.token = uuid4().hex
        # ensure token is unique
        while Invite.query.filter_by(token=self.token).first():
            self.token = uuid4().hex

    @classmethod
    def find(cls, **kwargs):
        return cls.query.filter_by(**kwargs).first()

    @classmethod
    def find_all(cls, **kwargs):
        return cls.query.filter_by(**kwargs).all()
