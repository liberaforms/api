"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import datetime
from uuid import uuid4
from sqlalchemy.dialects.postgresql import JSON
from sqlalchemy.orm import relationship
from werkzeug.security import ( generate_password_hash,
                                check_password_hash)

from api import db
from . import DictMixIn

class User(db.Model, DictMixIn):
    __tablename__ = "Users"

    id = db.Column(db.Integer, primary_key=True, index=True)
    createdAt = db.Column(db.DateTime)
    createdBy = db.Column(db.Integer, db.ForeignKey('Users.id'))
    username = db.Column(db.String, unique=True)
    email = db.Column(db.String, unique=True)
    # TODO Consider need to validate the email
    #is_email_validated = db.Column(db.Boolean, default=False)
    password_hash = db.Column(db.String)
    # TODO consider use enumtables
    ## comma separated Strings are valid ie: admin, user, owner
    group = db.Column(db.String)
    # TODO consider lazy="dynamic" to only query when needed
    invitations = relationship("Invite", back_populates="creator")

    def __init__(self, username, email, group, createdBy):
        self.username = username
        self.email = email
        self.group = group
        self.createdAt = datetime.datetime.now().isoformat()
        self.createdBy = createdBy

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def verify_password(self, password):
        return check_password_hash(self.password_hash, password)
