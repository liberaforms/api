"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import os, logging, traceback
from threading import Thread
import smtplib, ssl
from email.mime.multipart import MIMEMultipart
from email.mime.text import MIMEText
from email.utils import formatdate, make_msgid
from email.header import Header
from flask_babel import gettext
from flask import current_app

from pprint import pprint

class EmailServer():
    connection = None
    username = None
    password = None
    timeout = 7

    def load_config(self, **kwargs):
        self.host = kwargs['MAIL_SERVER']
        self.port = kwargs['MAIL_PORT']
        self.use_ssl = kwargs['MAIL_USE_SSL']
        self.use_tls = kwargs['MAIL_USE_TLS']
        self.username = kwargs['MAIL_USERNAME']
        self.password = kwargs['MAIL_PASSWORD']
        self.default_sender = kwargs['MAIL_DEFAULT_SENDER']

    def open_connection(self):
        if self.use_ssl:
            try:
                self.connection = smtplib.SMTP_SSL( host=self.host,
                                                    port=self.port,
                                                    timeout=self.timeout)
                self.connection.login(self.username, self.password)
            except Exception as error:
                logging.error(traceback.format_exc())
                self.connection = None
                return error
        # fix: STARTTLS does not always work
        elif self.use_tls:
            try:
                self.connection = smtplib.SMTP_SSL( host=self.host,
                                                    port=self.port,
                                                    timeout=self.timeout)
                context = ssl.create_default_context()
                self.connection.starttls(context=context)
                self.connection.login(self.username, self.password)
            except Exception as error:
                logging.error(traceback.format_exc())
                self.connection = None
                return error
        else:
            try:
                self.connection = smtplib.SMTP( host=self.host,
                                                port=self.port,
                                                timeout=self.timeout)
                if self.username and self.password:
                    self.connection.login(self.username, self.password)
            except Exception as error:
                logging.error(traceback.format_exc())
                self.connection = None
                return error


    def close_connection(self):
        """Closes the connection with the email server."""
        if self.connection is None:
            return
        try:
            try:
                self.connection.quit()
            except (ssl.SSLError, smtplib.SMTPServerDisconnected):
                # This happens when calling quit() on a TLS connection
                # sometimes, or when the connection was already disconnected
                # by the connection.
                self.connection.close()
            except smtplib.SMTPException as e:
                logging.error(traceback.format_exc())
        finally:
            self.connection = None


    def send_mail(self, msg):
        exception = self.open_connection()
        if exception:
            return {
                "email_sent": False,
                "msg": str(exception)
            }
        if self.connection:
            msg = self.prep_message(msg)
            try:
                self.connection.sendmail(   msg['From'],
                                            msg['To'],
                                            msg.as_string()
                                        )
                self.close_connection()
                return {
                    "email_sent": True
                }
            except Exception as error:
                logging.error(traceback.format_exc())
                return {
                    "email_sent": False,
                    "error": str(error)
                }
        return {
            "email_sent": False,
            "error": "Cannot connect to {}".format(self.host)
        }


    def send_mail_async(self, app, msg):
        with app.app_context():
            return self.send_mail(msg)


    def prep_message(self, msg):
        msg['From'] = self.default_sender
        msg['Date'] = formatdate(localtime=True)
        msg['Message-ID'] = make_msgid()
        """
        if not msg['Errors-To']:
            criteria={  'blocked': False,
                        'hostname': g.site.hostname,
                        'validatedEmail': True,
                        'admin__isAdmin': True }
                admins=User.find_all(**criteria)
                if admins:
                    msg['Errors-To'] = g.site.get_admins()[0].email
        """
        return msg

    def send_test_email(self, recipient_email):
        body = gettext("Congratulations!")
        msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
        msg['Subject'] = Header(gettext("MAIL test")).encode()
        msg['To'] = recipient_email
        #thr = Thread(   target=self.send_mail_async,
        #                args=[current_app._get_current_object(), msg])
        #thr.start()
        return self.send_mail(msg)

    def send_invite(self, invite, link):
        body = "{}\n\n{}".format(invite.message, link)
        msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
        msg['Subject'] = Header(gettext("Invitation to LiberaForms")).encode()
        msg['To'] = invite.email
        return self.send_mail(msg)

    def send_account_recovery(self, recipient_email, recovery_link):
        body = "{}\n\n{}\n\n{}".format(
                gettext("Please use this link to create a new password."),
                recovery_link,
                gettext("If you have not requested a new password you can ignore this email.")
                )
        msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
        header = Header(gettext("Recover password at LiberaForms"))
        msg['Subject'] = header.encode()
        msg['To'] = recipient_email
        return self.send_mail(msg)

    def send_password_success(self, recipient_email):
        body = gettext("Password reset successfully")
        msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
        header = Header(gettext("Password reset"))
        msg['Subject'] = header.encode()
        msg['To'] = recipient_email
        return self.send_mail(msg)

    def send_email_validation(self, recipient_email, link):
        body = gettext("Use this link to validate your email.")
        body = "{}\n\n{}".format(body, link)
        msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
        header = Header(gettext("Validate email"))
        msg['Subject'] = header.encode()
        msg['To'] = recipient_email
        return self.send_mail(msg)

    """
    def sendNewUserNotification(self, user):
        emails=[]
        criteria={  'blocked': False,
                    'hostname': user.hostname,
                    'validatedEmail': True,
                    'admin__isAdmin': True,
                    'admin__notifyNewUser': True}
        admins=User.find_all(**criteria)
        for admin in admins:
            emails.append(admin['email'])
        rootUsers=User.objects(__raw__={'email':{"$in": app.config['ROOT_USERS']},
                                        'admin.notifyNewUser':True})
        for rootUser in rootUsers:
            if not rootUser['email'] in emails:
                emails.append(rootUser['email'])
        body = gettext("New user '%s' created at %s" % (user.username, user.hostname))
        subject = Header(gettext("LiberaForms. New user notification")).encode()
        for msg_to in emails:
            msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
            msg['Subject'] = subject
            msg['To'] = msg_to
            self.send(msg)
        self.closeConnection()

    def sendRecoverPassword(self, user):
        link = "%ssite/recover-password/%s" % (g.site.host_url, user.token['token'])
        body = "%s\n\n%s" % (gettext("Please use this link to recover your password"), link)
        msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
        msg['Subject'] = Header(gettext("LiberaForms. Recover password")).encode()
        msg['To'] = user.email
        state = self.send(msg)
        self.closeConnection()
        return state

    def sendConfirmEmail(self, user, newEmail=None):
        link = "%suser/validate-email/%s" % (g.site.host_url, user.token['token'])
        body = gettext("Hello %s\n\nPlease confirm your email\n\n%s") % (user.username, link)
        msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
        msg['Subject'] = Header(gettext("LiberaForms. Confirm email")).encode()
        msg['To'] = newEmail if newEmail else user.email
        state = self.send(msg)
        self.closeConnection()
        return state

    def sendNewFormNotification(self, form):
        emails=[]
        criteria={  'blocked': False,
                    'hostname': form.hostname,
                    'validatedEmail': True,
                    'admin__isAdmin': True,
                    'admin__notifyNewForm': True}
        admins=User.find_all(**criteria)
        for admin in admins:
            emails.append(admin['email'])
        rootUsers=User.objects(__raw__={'email': {"$in": app.config['ROOT_USERS']},
                                        'admin.notifyNewForm':True})
        for rootUser in rootUsers:
            if not rootUser['email'] in emails:
                emails.append(rootUser['email'])

        body = gettext("New form '%s' created at %s" % (form.slug, form.hostname))
        subject = Header(gettext("LiberaForms. New form notification")).encode()
        for msg_to in emails:
            msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
            msg['Subject'] = subject
            msg['To'] = msg_to
            self.send(msg)
        self.closeConnection()

    def sendNewFormEntryNotification(self, emails, entry, slug):
        body = gettext("New form entry in %s at %s\n" % (slug, g.site.hostname))
        for data in entry:
            body = "%s\n%s: %s" % (body, data[0], data[1])
        body = "%s\n" % body
        subject = Header(gettext("LiberaForms. New form entry")).encode()
        for msg_to in emails:
            msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
            msg['Subject'] = subject
            msg['To'] = msg_to
            self.send(msg)
        self.closeConnection()

    def sendConfirmation(self, msg_to, form):
        msg = MIMEMultipart('alternative')
        html_body=MIMEText(form.after_submit_text_html, _subtype='html', _charset='UTF-8')
        msg.attach(html_body)
        msg['Subject'] = Header(gettext("Confirmation message")).encode()
        msg['To'] = msg_to
        state = self.send(msg)
        self.closeConnection()
        return state

    def sendExpiredFormNotification(self, editorEmails, form):
        body = gettext("The form '%s' has expired at %s" % (form.slug, g.site.hostname))
        subject = Header(gettext("LiberaForms. A form has expired")).encode()
        for msg_to in editorEmails:
            msg = MIMEText(body, _subtype='plain', _charset='UTF-8')
            msg['Subject'] = subject
            msg['To'] = msg_to
            self.send(msg)
        self.closeConnection()
    """
