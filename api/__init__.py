"""
Copyright 2020 LiberaForms.org

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
import os
from flask import Flask, Blueprint, session
from flask_jwt_extended import JWTManager
from flask_cors import CORS
from flask_sqlalchemy import SQLAlchemy
from flask_babel import Babel
from config import config
from api.utils.email import EmailServer

# app = Flask(__name__)

# Load defaults
# app.config.from_object(os.environ['APP_SETTINGS'])
# TODO study benefits
# print(os.environ['APP_SETTINGS'])
# User overrides
# app.config.from_pyfile("../config.cfg")

cors = CORS()
jwt = JWTManager()
db = SQLAlchemy()
babel = Babel()
email_server = EmailServer()


def create_app(config_name):
    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    cors.init_app(app, resources={r"/api/*": {"origins": "*"}})
    jwt.init_app(app)
    db.init_app(app)
    babel.init_app(app)

    email_server.load_config(**app.config)

    from api.routes import api_bp
    # TODO add /v1 suffix to API
    app.register_blueprint(api_bp, url_prefix='/api')

    return app
