import click
from flask.cli import with_appcontext
from api import db
from api.models.user import User

@click.command("create-user")
@click.argument("name")
@click.argument("email")
@click.argument("group")
@click.argument("password")
@with_appcontext
def create_user(name, email, group, password):
   user = User(name, email, group, createdBy=None)
   user.set_password(password)
   db.session.add(user)
   db.session.commit()
   print('User created: ', user.id)
