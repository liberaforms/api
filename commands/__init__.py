from commands.create_user import create_user


def register_commands(app):
  app.cli.add_command(create_user)
