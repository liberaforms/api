import subprocess

from autohooks.api import fail, ok
from autohooks.api.git import get_staged_status, stash_unstaged_changes
from autohooks.api.path import match

DEFAULT_IGNORE = [".autohooks/*"]
DEFAULT_INCLUDE = ["*.py"]


def get_ignore(config):
    if not config:
        return DEFAULT_IGNORE

    config = config.get("tool", "autohooks", "plugins", "prospector")
    return DEFAULT_IGNORE + config.get_value("ignore", [])


def get_include(config):
    if not config:
        return DEFAULT_INCLUDE

    config = config.get("tool", "autohooks", "plugins", "prospector")
    return DEFAULT_INCLUDE + config.get_value("include", [])


def precommit(**kwargs):
    config = kwargs.get("config")
    ignore = get_ignore(config)
    include = get_include(config)

    files = [
        f
        for f in get_staged_status()
        if match(f.path, include) and not match(f.path, ignore)
    ]

    if not files:
        # not files to lint
        return 0

    with stash_unstaged_changes(files):
        failed = False
        for file in files:
            status = subprocess.call(["prospector", "-P", "hook", str(file.path)])
            if status != 0:
                fail(f"Could not validate {str(file.path)}")
                failed = True
            else:
                ok(f"Validated {str(file.path)}")

        return 1 if failed else 0
