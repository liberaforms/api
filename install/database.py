#! /usr/bin/env python

"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import click
import os
from pathlib import Path
import signal, subprocess
from dotenv import load_dotenv

env_path = Path('../') / '.env'
load_dotenv(dotenv_path=env_path)

def run_subprocess(cmdline):
    try:
        p = subprocess.Popen(cmdline)
        p.wait()
    except KeyboardInterrupt:
        p.send_signal(signal.SIGINT)
        p.wait()

@click.group()
def cli():
    pass

@click.command()
@click.option('-docker', 'is_docker', is_flag=True, help="Use a docker container")
@click.option('-name', 'container_name', default=os.environ['DB_HOST'],
                help="PostgreSQL docker container name", show_default=True)
def create(is_docker, container_name):
    db_name = os.environ['DB_NAME']
    user = os.environ['DB_USER']
    password = os.environ['DB_PASSWORD']

    if is_docker:
        pg_cmd = f"docker exec {container_name} psql -U postgres -c".split()
    else:
        pg_cmd = "psql -U postgres -c ".split()

    sql = f"CREATE USER {user} WITH PASSWORD '{password}';"
    cmdline = pg_cmd + [sql]
    click.echo(" ".join(cmdline))
    run_subprocess(cmdline)

    sql = f"CREATE DATABASE {db_name} ENCODING 'UTF8' TEMPLATE template0;"
    cmdline = pg_cmd + [sql]
    click.echo(" ".join(cmdline))
    run_subprocess(cmdline)

    sql = f"GRANT ALL PRIVILEGES ON DATABASE {db_name} TO {user};"
    cmdline = pg_cmd + [sql]
    click.echo(" ".join(cmdline))
    run_subprocess(cmdline)

@click.command()
@click.option('-docker', 'is_docker', is_flag=True, help="Use a docker container")
@click.option('-name', 'container_name', default="lbfrms_api", show_default=True,
                help="LiberaForms API docker container name")
def init(is_docker, container_name):
    if is_docker:
        flask_cmd = f"docker exec {container_name} flask db".split()
    else:
        flask_cmd = "flask db".split()
    cmdline = flask_cmd + ['migrate']
    click.echo(" ".join(cmdline))
    run_subprocess(cmdline)
    cmdline = flask_cmd + ['upgrade']
    click.echo(" ".join(cmdline))
    run_subprocess(cmdline)


@click.command()
@click.option('-docker', 'is_docker', is_flag=True, help="Use a docker container")
@click.option('-name', 'container_name', default=os.environ['DB_HOST'],
                help="PostgreSQL docker container name")
def drop(is_docker, container_name):
    db_name = os.environ['DB_NAME']
    db_user = os.environ['DB_USER']

    if is_docker:
        pg_cmd = f"docker exec {container_name} psql -U postgres -c".split()
    else:
        pg_cmd = "psql -U postgres -c".split()

    sql = f"DROP DATABASE {db_name};"
    cmdline = pg_cmd + [sql]
    click.echo(" ".join(cmdline))
    run_subprocess(cmdline)

    sql = f"DROP USER IF EXISTS {db_user}"
    cmdline = pg_cmd + [sql]
    click.echo(" ".join(cmdline))
    run_subprocess(cmdline)


cli.add_command(create)
cli.add_command(init)
cli.add_command(drop)


if __name__ == "__main__":
    cli()
