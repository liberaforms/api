#! /usr/bin/env python

"""
“Copyright 2021 LiberaForms.org”

This file is part of LiberaForms.

LiberaForms is free software: you can redistribute it and/or modify
it under the terms of the GNU Affero General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""

import click
import os
from pathlib import Path
import signal, subprocess
from dotenv import load_dotenv

env_path = Path('../') / '.env'
load_dotenv(dotenv_path=env_path)

def run_subprocess(cmdline):
    try:
        p = subprocess.Popen(cmdline)
        p.wait()
    except KeyboardInterrupt:
        p.send_signal(signal.SIGINT)
        p.wait()

@click.group()
def cli():
    pass


@click.command()
@click.argument('username')
@click.argument('email')
@click.argument('group')
@click.argument('password')
@click.option('-docker', 'is_docker', is_flag=True, help="Use a docker container")
@click.option('-name', 'container_name', default="lbfrms_api",
                help="LiberaForms API docker container name")
def create(username, email, group, password, is_docker, container_name):

    if is_docker:
        flask_cmd = f"docker exec {container_name} flask create-user".split()
    else:
        flask_cmd = "flask create-user".split()

    cmdline = flask_cmd + [username, email, group, password]
    click.echo(" ".join(cmdline))
    run_subprocess(cmdline)


cli.add_command(create)


if __name__ == "__main__":
    cli()
