Libera forms API
================

[![Deploy](https://www.herokucdn.com/deploy/button.svg)](https://heroku.com/deploy?template=https://gitlab.com/liberaforms/api)


Project page [https://liberaforms.org](https://liberaforms.org)

We have built this software with the hope it will be used by our neighbours, friends, and anyone else who feels GAFAM already has way to much data on **all** of us.

Don't feed the Dictator!

Please read docs/INSTALL.txt for installation instructions.


## Getting started

An example of needed environment variables can be found in _dotenv.example_
Edit it and save it to _.env_

### pip

Run the project:

```bash
python3 -m venv venv
./venv/bin/activate
pip install -r requirements.txt
```

### [poetry](https://python-poetry.org/docs/#installation)

Run the project:

```bash
poetry install --no-root
. ./.env # if you don't load it automatically with dotenv or alike
poetry run flask run
```

If you plan to commit code changes add git hooks:

```bash
poetry run autohooks activate
```

## PostgreSQL

### Create the database

Create the database `DB_NAME` for LiberaForms.

```
cd install
./database.py create
```

### Populate the database

```
./database.py init
```

## Create an admin user

Supported groups are: `admin`, `user`
```
cd install
./user.py create <username> <email> <group> <password>
```

# Docker

Build the image

```
docker build -t liberaforms-api:latest .
```

## docker-compose

Revise your `.env` file.

Run Postgres server and LiberaForms api containers.

Copy a compose file from the `./docker-compose` directory

```
cp ./docker-compose/production-compose.yml ./docker-compose.yml
```
And then
```
docker-compose up
```

## Database

By default these commands assume the PostgreSQL container name is `DB_HOST`

### Create the database

When the database is server ready, create the database `DB_NAME` for LiberaForms.

```
cd install
./database.py create -docker
```
Optionally, define the PostgreSQL container name. See `--help` for default value.

List the container names
```
docker-compose ps
```
```
cd install
./database.py create -docker -name=<postgres_container_name>
```

### Populate the database
```
./database.py init -docker
```
Optionally, define the LiberaForms API container name
```
cd install
./database.py init -docker -name=<api_container_name>
```

## Create an admin user
Supported groups are: `admin`, `user`
```
cd install
./user.py create <username> <email> <group> <password> -docker
```
Optionally, define the LiberaForms API container name

# Development

## Available flask commands

A set of handy utilities can be run using the Flask CLI.

### Create an admin user

`create-user` creates a new user in the database. Supported
groups are: `admin`, `user`

```
$ flask create-user <username> <email> <group> <password>
```

## Migrations

We use [Alembic](https://github.com/sqlalchemy/alembic) for handling migrations

After making any changes to a Model in `api/models` a new
migration file needs to be created with:

```
flask db migrate
```

After you can execute the migration with:

```
flask db upgrade
```
