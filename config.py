import os
basedir = os.path.abspath(os.path.dirname(__file__))

def get_SQLALCHEMY_DATABASE_URI():
    user = os.environ['DB_USER']
    pswd = os.environ['DB_PASSWORD']
    host = os.environ['DB_HOST']
    dbase = os.environ['DB_NAME']
    port = os.environ.get('DB_PORT', 5432)
    uri = f'postgresql+psycopg2://{user}:{pswd}@{host}:{port}/{dbase}'
    return uri

class Config(object):
    DEBUG = False
    TESTING = False
    CSRF_ENABLED = False
    SECRET_KEY = 'this-really-needs-to-be-changed'
    SQLALCHEMY_DATABASE_URI = get_SQLALCHEMY_DATABASE_URI()
    SQLALCHEMY_TRACK_MODIFICATIONS = False

    RECOVER_PASSWORD_PATH = os.environ.get('RECOVER_PASSWORD_PATH', "/auth/recover")
    INVITES_PATH = os.environ.get('INVITES_PATH', "/invites")
    MAIL_SERVER = os.environ['MAIL_SERVER']
    MAIL_PORT = os.environ['MAIL_PORT']
    MAIL_USERNAME = os.environ['MAIL_USERNAME']
    MAIL_PASSWORD = os.environ['MAIL_PASSWORD']
    MAIL_USE_SSL = os.environ['MAIL_USE_SSL']
    MAIL_USE_TLS = os.environ['MAIL_USE_TLS']
    MAIL_DEFAULT_SENDER = os.environ['MAIL_DEFAULT_SENDER']


    @staticmethod
    def init_app(app):
        pass


class ProductionConfig(Config):
    DEBUG = False


class StagingConfig(Config):
    DEVELOPMENT = True
    #DEBUG = True  (not needed)


class DevelopmentConfig(Config):
    DEVELOPMENT = True


class TestingConfig(Config):
    TESTING = True


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'staging': StagingConfig,
    'production': ProductionConfig,
    'default': DevelopmentConfig
}
